﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrototioGG_SPERSON.DB.Departamento
{
    class DTODepartamento
    {
        public int ID { get; set; }

        public String Nome { get; set; }

        public decimal Salario { get; set; }
    }
}
