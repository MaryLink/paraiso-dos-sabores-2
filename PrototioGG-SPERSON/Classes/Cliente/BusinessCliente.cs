﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrototioGG_SPERSON.DB.Cliente
{
    class BusinessCliente
    {
        DatabaseCliente db = new DatabaseCliente();

        public int Salvar(DTOcliente dto)
        {
            if (dto.Nome == string.Empty)
            {
                throw new ArgumentException("Preencha o Campo Nome");
            }

            if (dto.Celular == string.Empty)
            {
                throw new ArgumentException("Preencha o Campo Celular");

            }

            if (dto.Telefone == string.Empty)
            {
                throw new ArgumentException("Preencha o Campo Telefone");

            }
            if (dto.Bairro == string.Empty)
            {
                throw new ArgumentException("Preencha o Campo Bairro");

            }
            if (dto.CEP == string.Empty)
            {
                throw new ArgumentException("Preencha o Campo CEP");

            }
            if (dto.Cidade == string.Empty)
            {
                throw new ArgumentException("Preencha o Campo Cidade");

            }
            if (dto.CPF == string.Empty)
            {
                throw new ArgumentException("Preencha o Campo CPF");
            }
            if (dto.Email == string.Empty)
            {
                throw new ArgumentException("Preencha o Campo Email");
            }
            if (dto.Estado == string.Empty)
            {
                throw new ArgumentException("Preencha o Campo Estado");
            }
            if (dto.Rua == string.Empty)
            {
                throw new ArgumentException("Preencha o Campo Rua");
            }
            return db.Salvar(dto);
        }

        public void Deletar(int id)
        {
            db.Deletar(id);
        }

        public void Alterar(DTOcliente dto)
        {
            db.Alterar(dto);
        }


        public List<DTOcliente> Consultar(string nome)
        {
            if (nome == "")
            {
                nome = string.Empty;

            }

            return db.Consultar(nome);

        }

        public List<DTOcliente> Listar()
        {
            return db.Listar();

        }
    }
}
