﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrototioGG_SPERSON.Classes.Funcionario.BaterPonto
{
    class DTOPonto
    {
        public DateTime Data { get; set; }

        public DateTime Atraso { get; set; }

        public DateTime Entrada { get; set; }

        public DateTime Saida { get; set; }

        public DateTime Extra { get; set; }

        public bool Almoco { get; set; }

        public int FK_Funcionario { get; set; }
    }
}
