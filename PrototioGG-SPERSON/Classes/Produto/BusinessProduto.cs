﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrototioGG_SPERSON.DB.Produto
{
    class BusinessProduto
    {
        DatabaseProduto db = new DatabaseProduto();

        public int Salvar(DTOProduto dto)
        {
            if (dto.Nome == string.Empty)
            {
                throw new ArgumentException("Informe o Nome do produto");
            }

            if (dto.Marca == string.Empty)
            {
                throw new ArgumentException("Informe a marca do produto");

            }

            if (dto.Imagem == string.Empty)
            {
                throw new ArgumentException("Selecione a Imagem");

            }

            if (dto.Valor_Unit_Compra == 0)
            {
                throw new ArgumentException("Informe o valor de compra do produto");

            }


            if (dto.Valor_Unit_Venda == 0)
            {
                throw new ArgumentException("Informe o valor de venda do produto");

            }
            return db.Salvar(dto);
        }

        public List<DTOProduto> Consultar(string nome)
        {
            if (nome == "")
            {
                nome = string.Empty;

            }

            return db.Consultar(nome);

        }

        public List<DTOProduto> Listar()
        {
            return db.Listar();

        }

        public DTOProduto ConsultarExato(string nome)
        {
            if (nome == "")
            {
                nome = string.Empty;

            }

            return db.ConsultarExato(nome);

        }

        public void Remover(int id)
        {
            db.Deletar(id);
        }

        public void Alterar(DTOProduto dto)
        {
            db.Alterar(dto);
        }


    }
}
