﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrototioGG_SPERSON.Classes.Estoque
{
    class DTOsupEstoque
    {
        public int ID { get; set; }

        public string Nome_Produto { get; set; }

        public int Quantidade { get; set; }
    }
}
