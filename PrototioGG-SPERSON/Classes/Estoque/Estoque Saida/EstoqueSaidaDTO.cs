﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrototioGG_SPERSON.Classes.Estoque.Estoque_Saida
{
     public class EstoqueSaidaDTO
    {
        public int Id_Estoque_Saida { get; set; }
        public int Data_Compra{ get; set; }
        public int Data_Entrega { get; set; }
        public int Fk_Venda { get; set; }
        public int Fk_Estoque { get; set; }



    }
}
