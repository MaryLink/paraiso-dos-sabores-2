﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrototioGG_SPERSON.Classes.Estoque
{
     public class EstoqueDTO
    {
        public int Id_Estoque { get; set; }

        public int Qtd_Atual { get; set;  }

        public int Qtd_Maxima { get; set; }

        public int Qtd_Minima { get; set; }

        public int Fk_Produto { get; set; }

   }
}
