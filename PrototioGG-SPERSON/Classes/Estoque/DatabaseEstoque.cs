﻿using MySql.Data.MySqlClient;
using PrototioGG_SPERSON.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrototioGG_SPERSON.Classes.Estoque
{
    class DatabaseEstoque
    {
        Database db = new Database();

        public EstoqueDTO Consultar(EstoqueDTO dto)
        {
            string script = @"SELECT * FROM tb_estoque WHERE fk_produto = @fk_produto";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("fk_produto", dto.Fk_Produto));

            Database db = new Database();

            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            if (reader.Read())
            {
                dto.Id_Estoque = reader.GetInt32("id_estoque");
                dto.Qtd_Atual = reader.GetInt32("qtd_atual");
                dto.Qtd_Maxima = reader.GetInt32("qtd_max");
                dto.Qtd_Minima = reader.GetInt32("qtd_min");
            }
            else
            {
                dto = null;
            }

            reader.Close();

            return dto;

        }

        }
}
