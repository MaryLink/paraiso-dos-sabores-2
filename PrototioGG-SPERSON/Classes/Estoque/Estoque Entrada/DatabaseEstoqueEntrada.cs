﻿using MySql.Data.MySqlClient;
using PrototioGG_SPERSON.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrototioGG_SPERSON.Classes.Estoque.Estoque_Entrada
{
    class DatabaseEstoqueEntrada
    {
        public void AtualizarEstoque(int qtd_atual, int qtd_comprada, int id_produto)
        {
            string script = @"UPDATE tb_estoque
                              SET qtd_atual = @qtd_atual + @qtd_comprada
                              WHERE fk_produto = @fk_produto";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("qtd_atual", qtd_atual));
            parms.Add(new MySqlParameter("qtd_comprada", qtd_comprada));
            parms.Add(new MySqlParameter("fk_produto", id_produto));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }

        public int SalvarEstoque(EstoqueDTO dto)
        {
            string script = @"INSERT tb_estoque(qtd_atual, qtd_max, qtd_min, fk_produto)Values (@qtd_atual, @qtd_max, @qtd_min, @fk_produto)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("qtd_atual", dto.Qtd_Atual));
            parms.Add(new MySqlParameter("qtd_max", dto.Qtd_Maxima));
            parms.Add(new MySqlParameter("qtd_min", dto.Qtd_Minima));
            parms.Add(new MySqlParameter("fk_produto", dto.Fk_Produto));

            Database db = new Database();
            int id = db.ExecuteInsertScriptWithPk(script, parms);

            return id;
        }

    }
}
