﻿using PrototioGG_SPERSON.DB.Produto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrototioGG_SPERSON.Classes.Estoque.Estoque_Entrada
{
    class BusinessEstoqueEntrada
    {
        DatabaseEstoqueEntrada db = new DatabaseEstoqueEntrada();
        public void AtualizarEstoque(int qtd_comprada, string nome_produto)
        {
            BusinessEstoque bussEs = new BusinessEstoque();
            EstoqueDTO dto = new EstoqueDTO();
            EstoqueDTO dto2 = new EstoqueDTO();
            BusinessProduto bussProd = new BusinessProduto();
            DTOProduto dtoProd = new DTOProduto();

            dtoProd = bussProd.ConsultarExato(nome_produto);

            dto.Fk_Produto = dtoProd.ID;

            dto2 = bussEs.Consultar(dto);
            if (dto2 == null)
            {
                dto.Qtd_Atual = 0;

                db.SalvarEstoque(dto);

                dto2 = bussEs.Consultar(dto);
            }

            db.AtualizarEstoque(dto2.Qtd_Atual, qtd_comprada, dtoProd.ID);


        }
    }
}
