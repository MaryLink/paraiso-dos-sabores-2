﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrototioGG_SPERSON.Classes.Compra.CompraItens.View
{
    class DTOvwCompra
    {
        public int ID { get; set; }

        public int Quantidade { get; set; }

        public string Forma_de_Pagamento { get; set; }

        public DateTime Data_Compra { get; set; }

        public decimal Valor_Total { get; set; }

        public string Nome_Fornecedor { get; set; }

        public string Nome_Produto { get; set; }
    }
}
