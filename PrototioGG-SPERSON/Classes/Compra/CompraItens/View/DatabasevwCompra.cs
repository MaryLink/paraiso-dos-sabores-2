﻿using MySql.Data.MySqlClient;
using PrototioGG_SPERSON.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrototioGG_SPERSON.Classes.Compra.CompraItens.View
{
    class DatabasevwCompra
    {
        public List<DTOvwCompra> ConsultarView(DTOvwCompra dto)
        {
            string script = @"SELECT * FROM   vw_consultar_compra
                                       WHERE  nm_produto   
                                        LIKE @nm_produto";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_produto", "%" + dto.Nome_Produto + "%"));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<DTOvwCompra> dto3 = new List<DTOvwCompra>();
            while (reader.Read())
            {
                DTOvwCompra dto2 = new DTOvwCompra();
                dto2.ID = reader.GetInt32("id_compra");
                dto2.Nome_Produto = reader.GetString("nm_produto");
                dto2.Quantidade = reader.GetInt32("qtd_produto");
                dto2.Valor_Total = reader.GetDecimal("vl_total");
                dto2.Forma_de_Pagamento = reader.GetString("forma_de_pagamento");
                dto2.Data_Compra = reader.GetDateTime("dt_compra");
                dto2.Nome_Fornecedor = reader.GetString("nm_fornecedor");


                dto3.Add(dto2);
            }
            reader.Close();
            return dto3;
        }
    }
}
