﻿using MySql.Data.MySqlClient;
using PrototioGG_SPERSON.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrototioGG_SPERSON.Classes.Compra
{
    class DatabaseCompra
    {
            public int Salvar(DTOCompra dto)
            {
                string script = @"INSERT INTO tb_compra (forma_de_pagamento, dt_compra, qtd_produto, vl_total, fk_fornecedor) VALUES (@forma_de_pagamento, @dt_compra, @qtd_produto, @vl_total, @fk_fornecedor)";

                List<MySqlParameter> parms = new List<MySqlParameter>();
                parms.Add(new MySqlParameter("forma_de_pagamento", dto.Forma_De_Pagamento));
                parms.Add(new MySqlParameter("dt_compra", dto.DT_Compra));
                parms.Add(new MySqlParameter("qtd_produto", dto.Qtd_Produtos));
                parms.Add(new MySqlParameter("vl_total", dto.VL_Total));
                parms.Add(new MySqlParameter("fk_fornecedor", dto.FK_Fornecedor));

                Database db = new Database();
                return db.ExecuteInsertScriptWithPk(script, parms);
            }

            public void Remover(int id)
            {
                string script = @"DELETE FROM tb_compra WHERE id_compra = @id_compra ";

                List<MySqlParameter> parms = new List<MySqlParameter>();
                parms.Add(new MySqlParameter("id_compra", id));

                Database db = new Database();
                db.ExecuteInsertScript(script, parms);
            }


        }
    
}
