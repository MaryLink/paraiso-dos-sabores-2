﻿using PrototioGG_SPERSON.Classes.Compra.CompraItens;
using PrototioGG_SPERSON.DB.Produto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrototioGG_SPERSON.Classes.Compra
{
    class BusinessCompra
    {
        public int Salvar(DTOCompra compra, List<DTOProduto> item)
        {
            DatabaseCompra compraDatabase = new DatabaseCompra();
            int idCompra = compraDatabase.Salvar(compra);

            BusinessCompraItem business = new BusinessCompraItem();
            foreach (DTOProduto itens in item)
            {
                DTOCompraItem itemDto = new DTOCompraItem();
                itemDto.FK_Compra = idCompra;
                itemDto.FK_Produto = itens.ID;

                business.Salvar_FKs(itemDto);
            }

            return idCompra;
        }
    }
}
