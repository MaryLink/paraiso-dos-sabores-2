﻿using MySql.Data.MySqlClient;
using PrototioGG_SPERSON.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrototioGG_SPERSON.Classes.Fluxo_de_Caixa
{
    class DatabaseFluxoDeCaixa
    {
        public List<DTOFluxoDeCaixa> ConsultarGanhos()
        {
            string script = @"SELECT * FROM vw_ganhos";


            Database db = new Database();

            MySqlDataReader reader = db.ExecuteSelectScript(script, null);

            List<DTOFluxoDeCaixa> lista = new List<DTOFluxoDeCaixa>();

            while (reader.Read())
            {
                DTOFluxoDeCaixa dto = new DTOFluxoDeCaixa();

                dto.Data = reader.GetDateTime("dt_referencia");
                dto.Valor = reader.GetDecimal("vl_total");

                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }

        public List<DTODespesas> ConsultarDespesas()
        {
            string script = @"SELECT * FROM vw_despesas";


            Database db = new Database();

            MySqlDataReader reader = db.ExecuteSelectScript(script, null);

            List<DTODespesas> lista = new List<DTODespesas>();

            while (reader.Read())
            {
                DTODespesas dto = new DTODespesas();

                dto.Data = reader.GetDateTime("dt_referencia");
                dto.Valor = reader.GetDecimal("vl_total");

                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }
    }
}
