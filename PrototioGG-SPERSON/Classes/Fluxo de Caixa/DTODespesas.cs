﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrototioGG_SPERSON.Classes.Fluxo_de_Caixa
{
    class DTODespesas
    {
        public DateTime Data { set; get; }

        public Decimal Valor { get; set; }
    }
}
