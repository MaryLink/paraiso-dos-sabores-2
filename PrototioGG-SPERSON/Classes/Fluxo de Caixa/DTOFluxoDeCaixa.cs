﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrototioGG_SPERSON.Classes.Fluxo_de_Caixa
{
    class DTOFluxoDeCaixa
    {
        public DateTime Data { get; set; }

        public Decimal Valor { get; set; }
    }
}
