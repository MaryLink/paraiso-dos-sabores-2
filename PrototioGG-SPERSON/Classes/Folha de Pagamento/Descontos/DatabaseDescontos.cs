﻿using MySql.Data.MySqlClient;
using PrototioGG_SPERSON.Classes.Folha_de_Pagamento.Descontos;
using PrototioGG_SPERSON.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrototioGG_SPERSON.Classes.Folha_de_Pagamento.Desconto
{
    class DatabaseDescontos
    {

        Database db = new Database();

        public int Salvar(DTODescontos dto)
        {
            string script = @"INSERT INTO tb_Descontos (Valor_Vale_Transporte, Valor_vale_Refeicao, Valor_FGTS)
                                VALUES (@Valor_Vale_Transporte @Valor_vale_Refeicao @Valor_FGTS)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("Valor_Vale_Transporte", dto.Valor_Vale_Transporte));
            parms.Add(new MySqlParameter("Valor_Vale_Refeicao", dto.Valor_Vale_Refeicao));
            parms.Add(new MySqlParameter("Valor_FGTS", dto.Valor_FGTS));

            int id = db.ExecuteInsertScriptWithPk(script, parms);
            return id;
        }

        public List<DTODescontos> Listar(string Descontos)
        {
            string script = @" SELECT * FROM tb_Descontos 
                                WHERE Valor_Vale_Transporte like @Valor_Vale_Transporte";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("Valor_Vale_Transporte", "%" + Descontos));

            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<DTODescontos> lista = new List<DTODescontos>();

            while (reader.Read())
            {
                DTODescontos dto = new DTODescontos();
                dto.Id_Descontos = reader.GetInt32("id_Descontos");
                dto.Valor_Vale_Transporte = reader.GetDecimal("Valor_Vale_Transporte");
                dto.Valor_Vale_Refeicao = reader.GetDecimal("Valor_Vale_Refeicao");
                dto.Valor_FGTS = reader.GetDecimal("Valor_FGTS");

                lista.Add(dto);

            }
            reader.Close();

            return lista;


        }
        public List<DTODescontos> Listar2()
        {
            string script = @" SELECT * FROM tb_Descontos";

            MySqlDataReader reader = db.ExecuteSelectScript(script, null);

            List<DTODescontos> lista = new List<DTODescontos>();

            while (reader.Read())
            {
                DTODescontos dto = new DTODescontos();
                dto.Id_Descontos = reader.GetInt32("id_Descontos");
                dto.Valor_Vale_Transporte = reader.GetDecimal("Valor_Vale_Transporte");
                dto.Valor_Vale_Refeicao = reader.GetDecimal("Valor_Vale_Refeicao");
                dto.Valor_FGTS = reader.GetDecimal("Valor_FGTS");



                lista.Add(dto);

            }
            reader.Close();

            return lista;



        }
    }
}
