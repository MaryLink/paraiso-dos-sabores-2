﻿using MySql.Data.MySqlClient;
using PrototioGG_SPERSON.Classes.Funcionario.BaterPonto;
using PrototioGG_SPERSON.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrototioGG_SPERSON.Classes.Folha_de_Pagamento.Ponto
{
    class DatabasePonto
    {
        Database db = new Database();

        public int Salvar(DTOPonto dto)
        {
            string script = @"INSERT INTO tb_ponto (dt_ponto, dt_atraso, hr_extra, hr_jornada_trabalho_inicio, hr_jornada_trabalho_fim, bl_almoco, fk_funcionario) VALUES (@dt_ponto, @dt_atraso, @hr_extra, @hr_jornada_trabalho_inicio, @hr_jornada_trabalho_fim, @bl_almoco,@fk_funcionario)";


            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("dt_ponto", dto.Data));
            parms.Add(new MySqlParameter("dt_atraso", dto.Atraso));
            parms.Add(new MySqlParameter("hr_extra", dto.Extra));
            parms.Add(new MySqlParameter("hr_jornada_trabalho_inicio", dto.Entrada));
            parms.Add(new MySqlParameter("hr_jornada_trabalho_fim", dto.Saida));
            parms.Add(new MySqlParameter("bl_almoco", dto.Almoco));
            parms.Add(new MySqlParameter("fk_funcionario", dto.FK_Funcionario));

            Database db = new Database();
            int id = db.ExecuteInsertScriptWithPk(script, parms);

            return id;
        }

        //public List<DTOPonto> Consultar(string Ponto)
        //{
        //    string script = @" SELECT * FROM tb_Ponto 
        //                        WHERE Data_Dia like @Data_Dia";

        //    List<MySqlParameter> parms = new List<MySqlParameter>();
        //    parms.Add(new MySqlParameter("Data_Dia", "%" + Ponto));

        //    MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

        //    List<DTOPonto> lista = new List<DTOPonto>();

        //    while (reader.Read())
        //    {
        //        DTOPonto dto = new DTOPonto();
        //        dto.id_Ponto = reader.GetInt32("id_Ponto");
        //        dto.Data_Dia = reader.GetDateTime("Data_Dia");
        //        dto.Data_Atraso = reader.GetDateTime("Data_Atraso");
        //        dto.Hora_Entrada = reader.GetDateTime("Hora_Entrada");
        //        dto.Hora_Saida = reader.GetDateTime("Hora_Saida");
        //        dto.Hora_Extra = reader.GetDateTime("Hora_Extra");
        //        dto.Hora_Jornada_Trabalho_Inicio = reader.GetDateTime("Hora_Jornada_Trabalho_Inicio");
        //        dto.Hora_Jornada_Trabalho_Fim = reader.GetDateTime("Hora_Jornada_Trabalho_Fim");
        //        dto.Fk_Funcionario = reader.GetInt32("Fk_Funcionario");

        //        lista.Add(dto);

        //    }
        //    reader.Close();

        //    return lista;


        //}
        //public List<DTOPonto> Listar2()
        //{
        //    string script = @" SELECT * FROM tb_Ponto";

        //    MySqlDataReader reader = db.ExecuteSelectScript(script, null);

        //    List<DTOPonto> lista = new List<DTOPonto>();

        //    while (reader.Read())
        //    {
        //        DTOPonto dto = new DTOPonto();
        //        dto.id_Ponto= reader.GetInt32("id_Ponto");
        //        dto.Data_Dia = reader.GetDateTime("Data_Dia");
        //        dto.Data_Atraso = reader.GetDateTime("Data_Atraso");
        //        dto.Hora_Entrada = reader.GetDateTime("Hora_Entrada");
        //        dto.Hora_Saida = reader.GetDateTime("Hora_Saida");
        //        dto.Hora_Extra = reader.GetDateTime("Hora_Extra");
        //        dto.Hora_Jornada_Trabalho_Inicio = reader.GetDateTime("Hora_Jornada_Trabalho_Inicio");
        //        dto.Hora_Jornada_Trabalho_Fim = reader.GetDateTime("Hora_Jornada_Trabalho_Fim");
        //        dto.Fk_Funcionario = reader.GetInt32("Fk_Funcionario");



        //        lista.Add(dto);

        //    }
        //    reader.Close();

        //    return lista;
        //}
        //public List<PontoDTO> Listar (DateTime dia, DateTime atraso)
        //   {
        //       string script = @"SELECT * FROM tb_Ponto
        //                                  WHERE Data_dia >= @dia AND Data_atraso <= atraso";

        //   }

    }

}
