﻿using MySql.Data.MySqlClient;
using PrototioGG_SPERSON.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrototioGG_SPERSON.Classes.Folha_de_Pagamento.Inss
{
    class DatabaseInss
    {
        Database db = new Database();

        public int Salvar(DTOInss dto)
        {
            string script = @"INSERT INTO tb_inss (Valor_Faixa_Salario, Valor_Aliquota)
                                VALUES (@Valor_Faixa_Salario, @Valor_Aliquota)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("Valor_Faixa_Salario", dto.Valor_Faixa_Salario));
            parms.Add(new MySqlParameter("Valor_Aliquota", dto.Valor_Aliquota));

            int id = db.ExecuteInsertScriptWithPk (script, parms);
            return id;
        }

        public List<DTOInss> Consultar(string inss)
        {
            string script = @" SELECT * FROM tb_inss 
                                WHERE Valor_Faixa_Salario like @Valor_Faixa_Salario";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("Valor_Faixa_Salario", "%" + inss));

            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<DTOInss> lista = new List<DTOInss>();

            while (reader.Read())
            {
                DTOInss dto = new DTOInss();
                dto.Id_Inss = reader.GetInt32("id_Inss");
                dto.Valor_Faixa_Salario = reader.GetDecimal("Valor_Faixa_Salario");
                dto.Valor_Aliquota = reader.GetDecimal("Valor_Aliquota");

                lista.Add(dto);

            }
            reader.Close();

            return lista;


        }
        public List<DTOInss> Listar()
        {
            string script = @" SELECT * FROM tb_Inss";

            MySqlDataReader reader = db.ExecuteSelectScript(script, null);

            List<DTOInss> lista = new List<DTOInss>();

            while (reader.Read())
            {
                DTOInss dto = new DTOInss();
                dto.Id_Inss = reader.GetInt32("id_Inss");
                dto.Valor_Faixa_Salario = reader.GetDecimal("Valor_Faixa_Salario");
                dto.Valor_Aliquota = reader.GetDecimal("Valor_Aliquota");



                lista.Add(dto);

            }
            reader.Close();

            return lista;








        }
    }
}