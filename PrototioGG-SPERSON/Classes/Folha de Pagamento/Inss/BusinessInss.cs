﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrototioGG_SPERSON.Classes.Folha_de_Pagamento.Inss
{
    class BusinessInss
    {
        DatabaseInss db = new DatabaseInss();

        public int Salvar(DTOInss dto)
        {
            if (dto.Valor_Faixa_Salario == 0)
            {
                throw new ArgumentException("Informe o Valor da faixa de Salário");
            }

            if (dto.Valor_Aliquota == 0)
            {
                throw new ArgumentException("Informe o valor Aliquota");

            }

            return db.Salvar(dto);
        }



        public List<DTOInss> Consultar (string inss )
        {
            if(inss == "")
            {
                inss = string.Empty;

            }

            return db.Consultar(inss);
        }

        public List<DTOInss> Listar ()
        {
            return db.Listar();

        }
    }

}

