﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrototioGG_SPERSON.Classes.Folha_de_Pagamento
{
    class CalcularFolhaDePagamento
    {
        public decimal ValorMensal(decimal salarioBruto, int totalDias, int diasTrabalhados)
        {
            decimal mes_valor = (salarioBruto / totalDias) * diasTrabalhados;

            return mes_valor;
        }

        public decimal ValorHora(decimal salarioBruto)
        {
            int n = 220;
            decimal total = salarioBruto / n;
            return total;
        }

        public decimal ValorHora50(decimal salarioBruto)
        {
            decimal valorhora = ValorHora(salarioBruto);
            decimal total = valorhora + (valorhora * (50 / 100));
            return total;
        }

        public decimal ValorHora100(decimal salarioBruto)
        {
            decimal valorhora = ValorHora(salarioBruto);
            decimal total = valorhora + (valorhora * (100 / 100));
            return total;
        }

        public decimal ValorHorasTrabalhadas50(decimal horasTrabalhadas50, decimal salarioBruto)
        {
            decimal valorhora = ValorHora(salarioBruto);

            decimal total = horasTrabalhadas50 * valorhora;
            return total;

        }

        public decimal ValorHorasTrabalhadas100(decimal horasTrabalhadas100, decimal salarioBruto)
        {
            decimal valorhora = ValorHora(salarioBruto);

            decimal total = horasTrabalhadas100 * valorhora;
            return total;

        }

        public decimal CalcularTotalProventos(decimal salarioBruto, int totalDias, int diasTrabalhados, decimal deducaoIR)
        {
            decimal mesValor = ValorMensal(salarioBruto, totalDias, diasTrabalhados);
            decimal horasTrabalhadas50 = ValorHora50(salarioBruto);
            decimal valorTrab50 = ValorHorasTrabalhadas50(horasTrabalhadas50, salarioBruto);
            decimal horasTrabalhadas100 = ValorHora100(salarioBruto);
            decimal valorTrab100 = ValorHorasTrabalhadas100(horasTrabalhadas100, salarioBruto);

            decimal ToltalDeProventos = mesValor + valorTrab50 + valorTrab100 + deducaoIR;
            return deducaoIR;
        }
    }
}
