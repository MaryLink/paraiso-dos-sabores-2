﻿using PrototioGG_SPERSON.Classes.Venda.VendaItens.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrototioGG_SPERSON.Classes.Folha_de_Pagamento.Comissao
{
    class CalculoComissao
    {
        public decimal CalcularComissao(decimal percentComissao, string nome_funcionario)
        {
            BusinessvwVenda buss = new BusinessvwVenda();

            DTOvwVenda dto = new DTOvwVenda();
            dto.nm_funcionario = nome_funcionario;

            List<DTOvwVenda> lista = new List<DTOvwVenda>();
            lista = buss.ConsultarViewFunc(dto);

            decimal valor_comissao = 0;
            percentComissao = 2.5m;
            foreach (DTOvwVenda item in lista)
            {
                decimal unit_price = item.vl_unitario_venda;
                decimal qtd = item.qtd_produto;
                decimal vl_total = unit_price * qtd;

                valor_comissao += vl_total * (percentComissao / 100);
            }

            return valor_comissao;
        }
    }
}
