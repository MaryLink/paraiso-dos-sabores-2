﻿	DROP DATABASE ggs_person;
	CREATE DATABASE ggs_person;
	USE ggs_person;

	CREATE TABLE `tb_estoque` (
		`id_estoque` INT NOT NULL AUTO_INCREMENT,
		`qtd_atual` INT NOT NULL,
		`qtd_max` INT NOT NULL,
		`qtd_min` INT NOT NULL,
		`fk_produto` INT NOT NULL,
		PRIMARY KEY (`id_estoque`)
	);

	CREATE TABLE `tb_fornecedor` (
		`id_fornecedor` INT NOT NULL AUTO_INCREMENT,
		`nm_fornecedor` varchar(100) NOT NULL,
		`ds_cnpj` varchar(20) NOT NULL,
		`ds_rua` varchar(50) NOT NULL,
		`ds_bairro` varchar(50) NOT NULL,
		`ds_cidade` varchar(50) NOT NULL,
		`ds_estado` varchar(20) NOT NULL,
		`ds_cep` varchar(12) NOT NULL,
		`ds_email` varchar(100) NOT NULL,
		`num_tel` varchar(20) NOT NULL,
		PRIMARY KEY (`id_fornecedor`)
	);

	CREATE TABLE `tb_produto` (
		`id_produto` INT NOT NULL AUTO_INCREMENT,
		`nm_produto` varchar(100) NOT NULL,
		`ds_produto` varchar(100) NOT NULL,
		`ds_categoria` varchar(100) NOT NULL,
		`vl_unitario_venda` DECIMAL(10,2) NOT NULL,
		`vl_unitario_compra` DECIMAL(10,2) NOT NULL,
		`ds_marca` varchar(100) NOT NULL,
		PRIMARY KEY (`id_produto`)
	);

	CREATE TABLE `tb_controle_estoque_entrada` (
		`id_controle_estoque` INT NOT NULL AUTO_INCREMENT,
		`dt_compra` INT NOT NULL,
		`dt_entrega` INT NOT NULL,
		`fk_compra` INT NOT NULL,
		`fk_estoque` INT NOT NULL,
		PRIMARY KEY (`id_controle_estoque`)
	);

	CREATE TABLE `tb_compra` (
		`id_compra` INT NOT NULL AUTO_INCREMENT,
		`vl_total` DECIMAL(10,2) NOT NULL,
		`qtd_produtos` INT NOT NULL,
		`fk_fornecedor` INT NOT NULL,
		PRIMARY KEY (`id_compra`)
	);

	CREATE TABLE `tb_venda` (
		`id_venda` INT NOT NULL AUTO_INCREMENT,
		`vl_total` DECIMAL(10,2) NOT NULL,
		`qtd_produtos` INT NOT NULL,
		`fk_cliente` INT NOT NULL,
		`fk_funcionario` INT NOT NULL,
		PRIMARY KEY (`id_venda`)
	);

	CREATE TABLE `tb_compra_item` (
		`id_compra_item` INT NOT NULL AUTO_INCREMENT,
		`fk_produto` INT NOT NULL,
		`fk_compra` INT NOT NULL,
		PRIMARY KEY (`id_compra_item`)
	);
	CREATE TABLE `tb_funcionario` (
		`id_funcionario` INT NOT NULL AUTO_INCREMENT,
		`nm_funcionario` varchar(50) NOT NULL,
		`ds_cpf` varchar(15) NOT NULL,
		`ds_rg` varchar(15) NOT NULL,
		`num_tel` varchar(15) NOT NULL,
		`num_cel` varchar(15) NOT NULL,
		`ds_rua` varchar(50) NOT NULL,
		`ds_bairro` varchar(50) NOT NULL,
		`ds_cidade` varchar(50) NOT NULL,
		`ds_estado` varchar(20) NOT NULL,
		`ds_cep` varchar(20) NOT NULL,
		`ds_username` varchar(50) NOT NULL,
		`ds_password` varchar(50) NOT NULL,
		`ds_email` varchar(50) NOT NULL,
		`fk_departamento` INT NOT NULL,
		`dt_nascimento` DATETIME NOT NULL,
		PRIMARY KEY (`id_funcionario`)
	);

	CREATE TABLE `tb_controle_estoque_saida` (
		`id_controle_estoque` INT NOT NULL AUTO_INCREMENT,
		`dt_compra` INT NOT NULL,
		`dt_entrega` INT NOT NULL,
		`fk_venda` INT NOT NULL,
		`fk_estoque` INT NOT NULL,
		PRIMARY KEY (`id_controle_estoque`)
	);

	CREATE TABLE `tb_departamento` (
		`id_departamento` INT NOT NULL AUTO_INCREMENT,
		`nm_departamento` varchar(50) NOT NULL,
		`vl_salario` DECIMAL(10,2) NOT NULL,
		PRIMARY KEY (`id_departamento`)
	);

	CREATE TABLE `tb_venda_item` (
		`id_venda_item` INT NOT NULL AUTO_INCREMENT,
		`fk_produto` INT NOT NULL,
		`fk_venda` INT NOT NULL,
		PRIMARY KEY (`id_venda_item`)
	);

	CREATE TABLE `tb_cliente` (
		`id_cliente` INT NOT NULL AUTO_INCREMENT,
		`nm_cliente` varchar(50) NOT NULL,
		`ds_cpf` varchar(15) NOT NULL,
		`num_tel` varchar(15) NOT NULL,
		`num_cel` varchar(15) NOT NULL,
		`ds_email` varchar(50) NOT NULL,
		`ds_rua` varchar(50) NOT NULL,
		`ds_bairro` varchar(50) NOT NULL,
		`ds_cidade` varchar(50) NOT NULL,
		`ds_estado` varchar(20) NOT NULL,
		`ds_cep` varchar(12) NOT NULL,
		PRIMARY KEY (`id_cliente`)
	);

	CREATE TABLE `tb_folha_pagamento` (
		`id_folha_pagamento` INT NOT NULL AUTO_INCREMENT,
		`vl_hr` DECIMAL(10,2) NOT NULL,
		`vl_vt` DECIMAL(10,2) NOT NULL,
		`vl_vr` DECIMAL(10,2) NOT NULL,
		`vl_fgts` DECIMAL(10,2) NOT NULL,
		`vl_inss` DECIMAL(10,2) NOT NULL,
		`vl_bruto` DECIMAL(10,2) NOT NULL,
		`vl_liquido` DECIMAL(10,2) NOT NULL,
		`vl_imposto_renda` DECIMAL(10,2) NOT NULL,
		`fk_ponto` INT NOT NULL,
		PRIMARY KEY (`id_folha_pagamento`)
	);

	CREATE TABLE `tb_comissao` (
		`Id_comissao` INT NOT NULL AUTO_INCREMENT,
		`fk_venda` INT NOT NULL,
		`fk_departamento` INT NOT NULL,
		`porcent_comissão` DECIMAL(10,2) NOT NULL,
		`vl_total_comissao` DECIMAL(10,2) NOT NULL,
		PRIMARY KEY (`Id_comissao`)
	);

	CREATE TABLE `tb_ponto` (
		`id_ponto` INT NOT NULL AUTO_INCREMENT,
		`dt_dia` DATE NOT NULL,
		`dt_atraso` DATE NOT NULL,
		`hr_entrada` TIME NOT NULL,
		`hr_saida` TIME NOT NULL,
		`hr_extra` TIME NOT NULL,
		`hr_jornada_trabalho_inicio` TIME NOT NULL,
		`hr_jornada_trabalho_fim` TIME NOT NULL,
		`fk_funcionario` INT NOT NULL,
		PRIMARY KEY (`id_ponto`)
	);


	  

	ALTER TABLE `tb_controle_estoque_entrada` ADD CONSTRAINT `tb_controle_estoque_entrada_fk0` FOREIGN KEY (`fk_compra`) REFERENCES `tb_compra`(`id_compra`);

	ALTER TABLE `tb_controle_estoque_entrada` ADD CONSTRAINT `tb_controle_estoque_entrada_fk1` FOREIGN KEY (`fk_estoque`) REFERENCES `tb_estoque`(`id_estoque`);

	ALTER TABLE `tb_compra` ADD CONSTRAINT `tb_compra_fk0` FOREIGN KEY (`fk_fornecedor`) REFERENCES `tb_fornecedor`(`id_fornecedor`);

	ALTER TABLE `tb_venda` ADD CONSTRAINT `tb_venda_fk0` FOREIGN KEY (`fk_cliente`) REFERENCES `tb_cliente`(`id_cliente`);

	ALTER TABLE `tb_venda` ADD CONSTRAINT `tb_venda_fk1` FOREIGN KEY (`fk_funcionario`) REFERENCES `tb_funcionario`(`id_funcionario`);

	ALTER TABLE `tb_compra_item` ADD CONSTRAINT `tb_compra_item_fk0` FOREIGN KEY (`fk_produto`) REFERENCES `tb_produto`(`id_produto`);

	ALTER TABLE `tb_compra_item` ADD CONSTRAINT `tb_compra_item_fk1` FOREIGN KEY (`fk_compra`) REFERENCES `tb_compra`(`id_compra`);

	ALTER TABLE `tb_funcionario` ADD CONSTRAINT `tb_funcionario_fk0` FOREIGN KEY (`fk_departamento`) REFERENCES `tb_departamento`(`id_departamento`);

	ALTER TABLE `tb_controle_estoque_saida` ADD CONSTRAINT `tb_controle_estoque_saida_fk0` FOREIGN KEY (`fk_venda`) REFERENCES `tb_venda`(`id_venda`);

	ALTER TABLE `tb_controle_estoque_saida` ADD CONSTRAINT `tb_controle_estoque_saida_fk1` FOREIGN KEY (`fk_estoque`) REFERENCES `tb_estoque`(`id_estoque`);

	ALTER TABLE `tb_venda_item` ADD CONSTRAINT `tb_venda_item_fk0` FOREIGN KEY (`fk_produto`) REFERENCES `tb_produto`(`id_produto`);

	ALTER TABLE `tb_venda_item` ADD CONSTRAINT `tb_venda_item_fk1` FOREIGN KEY (`fk_venda`) REFERENCES `tb_venda`(`id_venda`);

	ALTER TABLE `tb_folha_pagamento` ADD CONSTRAINT `tb_folha_pagamento_fk0` FOREIGN KEY (`fk_ponto`) REFERENCES `tb_ponto`(`id_ponto`);

	ALTER TABLE `tb_comissao` ADD CONSTRAINT `tb_comissao_fk0` FOREIGN KEY (`fk_venda`) REFERENCES `tb_venda`(`id_venda`);

	ALTER TABLE `tb_comissao` ADD CONSTRAINT `tb_comissao_fk1` FOREIGN KEY (`fk_departamento`) REFERENCES `tb_departamento`(`id_departamento`);

	ALTER TABLE `tb_ponto` ADD CONSTRAINT `tb_ponto_fk0` FOREIGN KEY (`fk_funcionario`) REFERENCES `tb_funcionario`(`id_funcionario`);

	select * from tb_produto;
	describe tb_produto;

	INSERT INTO tb_produto(nm_produto, ds_produto, ds_categoria, vl_unitario_compra, vl_unitario_venda, ds_marca) VALUES ('nome1', 'desc', 'desc categ1', 1, 2, 'desc marca1'),
	('nome2', 'desc2', 'desc categ2', 2, 4, 'desc marca2'),
	('nome3', 'desc3', 'desc categ3', 3, 4, 'desc marca3'),
	('nome4', 'desc4', 'desc categ4', 4, 4, 'desc marca4'),
	('nome5', 'desc5', 'desc categ5', 5, 4, 'desc marca5'),
	('nome6', 'desc6', 'desc categ6', 6, 4, 'desc marca6'),
	('nome7', 'desc7', 'desc categ7', 7, 4, 'desc marca7'),
	('nome8', 'desc8', 'desc categ8', 8, 4, 'desc marca8');

	insert into tb_fornecedor(nm_fornecedor, ds_cnpj, ds_rua, ds_bairro, ds_cidade, ds_estado, ds_cep, ds_email, num_tel)
	  values ('Nome1', '132132', 'rua1', 'bairro1', 'cidade1', 'estado1', 'cep1', 'email1', 'telefone1'),
			 ('Nome2' , '1332 ', 'rua2' ,'bairro2' , 'cidade2' , 'estado2 ' , 'cep2', 'email2' , 'telefone2'),
			 ('Nome3' , '2132 ', 'rua3' ,'bairro3' , 'cidade3' , 'estado3 ' , 'cep3', 'email3' , 'telefone3');

	 insert into tb_cliente (nm_cliente, ds_cpf, num_tel, num_cel, ds_email, ds_rua, ds_bairro, ds_cidade, ds_estado, ds_cep) Values (
	'nome1', 'cpf1', 'telefone1', 'celular1', 'email1', 'rua1', 'bairro1', 'cidade1', 'estado1', 'cep1'),
	('nome2', 'cpf2', 'telefone2', 'celular2', 'email2', 'rua2', 'bairro2', 'cidade2', 'estado2', 'cep2'),
	('nome3', 'cpf3', 'telefone3', 'celular3', 'email3', 'rua3', 'bairro3', 'cidade3', 'estado3', 'cep3'),
	('nome4', 'cpf4', 'telefone4', 'celular4', 'email4', 'rua4', 'bairro4', 'cidade4', 'estado4', 'cep4'),
	('nome5', 'cpf5', 'telefone5', 'celular5', 'email5', 'rua5', 'bairro5', 'cidade5', 'estado5', 'cep5'),
	('nome6', 'cpf6', 'telefone6', 'celular6', 'email6', 'rua6', 'bairro6', 'cidade6', 'estado6', 'cep6'),
	('nome7', 'cpf7', 'telefone7', 'celular7', 'email7', 'rua7', 'bairro7', 'cidade7', 'estado7', 'cep7'),
	('nome8', 'cpf8', 'telefone8', 'celular8', 'email8', 'rua8', 'bairro8', 'cidade8', 'estado8', 'cep8'),
	('nome9', 'cpf9', 'telefone9', 'celular9', 'email9', 'rua9', 'bairro9', 'cidade9', 'estado9', 'cep9'),
	('nome10', 'cpf10', 'telefone10', 'celular10', 'email10', 'rua10', 'bairro10', 'cidade10', 'estado10', 'cep10');

	insert into tb_departamento(nm_departamento, vl_salario) values ('ADM', 9999999.999);

	insert into tb_funcionario (nm_funcionario,
	  ds_cpf,
	  ds_rg,
	  num_tel,
	  num_cel,
	  ds_rua,
	  ds_bairro,
	  ds_cidade,
	  ds_estado,
	  ds_cep,
	  ds_username,
	  ds_password,
	  ds_email,
	  fk_departamento,
	  dt_nascimento)
	  values 
	  ('nome1', 'cpf1', 'rg1', 'telefone1', 'celular1' , 'rua1' , 'bairro1', 'cidade1', 'estado1', 'cep1', 'ADM' ,'ADM123', 'ADM@hotmail.com', 1, '2001-02-01'),
	  ('nome2', 'cpf2', 'rg2', 'telefone2', 'celular2' , 'rua2' , 'bairro2', 'cidade2', 'estado2', 'cep2', 'user2' ,'senha2', 'email2', 1, '2001-02-01'),
	  ('nome3', 'cpf3', 'rg3', 'telefone3', 'celular3' , 'rua3' , 'bairro3', 'cidade3', 'estado3', 'cep3', 'user3' ,'senha3', 'email3', 1,'2001-02-01'),
	  ('nome4', 'cpf4', 'rg4', 'telefone4', 'celular4' , 'rua4' , 'bairro4', 'cidade4', 'estado4', 'cep4',  'user4' ,'senha4', 'email4', 1, '2001-02-01'),
	  ('nome5', 'cpf5', 'rg5', 'telefone5', 'celular5' , 'rua5' , 'bairro5', 'cidade5', 'estado5', 'cep5',  'user5' ,'senha5', 'email5', 1, '2001-02-01');
      
	select * from tb_departamento;
	select * from tb_produto;
	select * from tb_departamento;
	select * from tb_venda;
	select * from tb_funcionario;
	select * from tb_compra;

     
	Create view vw_funcionario_departamento as 
		Select tb_funcionario.nm_funcionario,
			   tb_funcionario.ds_rg,
			   tb_funcionario.num_tel,
			   tb_funcionario.num_cel,
			   tb_funcionario.ds_email,
			   tb_departamento.vl_salario,
			   tb_departamento.nm_departamento,
			   tb_funcionario.dt_nascimento,
			   tb_funcionario.ds_cep,
			   tb_funcionario.ds_bairro,
			   tb_funcionario.ds_rua,
			   tb_funcionario.ds_cidade,
			   tb_funcionario.ds_estado
		From tb_funcionario
		join tb_departamento
		  on tb_funcionario.fk_departamento = tb_departamento.id_departamento;
			   
			   select * from vw_funcionario_departamento;
			   SELECT * FROM   ggs_person.vw_funcionario_departamento;