﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PrototioGG_SPERSON.DB.Cliente;

namespace PrototioGG_SPERSON.Telas.Alterar
{
    public partial class FrmClienteAlterar : UserControl
    {
        public FrmClienteAlterar()
        {
            InitializeComponent();
        }


        public void LoadScreen(DTOcliente dto)
        {
            lblID.Text = dto.ID.ToString();
            txtNome.Text = dto.Nome;
            txtBairro.Text = dto.Bairro;
            txtCelular.Text = dto.Celular;
            txtCEP.Text = dto.CEP;
            txtCidade.Text = dto.Cidade;
            txtCPF.Text = dto.CPF;
            txtEmail.Text = dto.Email;
            txtEstado.Text = dto.Estado;
            txtRua.Text = dto.Rua;
            txtTelefone.Text = dto.Telefone;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {

                DTOcliente dto = new DTOcliente();
                dto.ID = Convert.ToInt32(lblID.Text);
                dto.Nome = txtNome.Text;
                dto.Bairro = txtBairro.Text;
                dto.Rua = txtRua.Text;
                dto.Estado = txtEstado.Text;
                dto.Cidade = txtCidade.Text;
                dto.CPF = txtCPF.Text;
                dto.Telefone = txtTelefone.Text;
                dto.Celular = txtCelular.Text;
                dto.CEP = txtCEP.Text;
                dto.Email = txtEmail.Text;

                BusinessCliente bus = new BusinessCliente();
                bus.Alterar(dto);
            }

            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }


            MessageBox.Show("Alterado com sucesso");
        }
    }
}
