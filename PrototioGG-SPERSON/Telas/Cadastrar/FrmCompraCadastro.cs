﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PrototioGG_SPERSON.DB.Fornecedor;
using PrototioGG_SPERSON.DB.Produto;
using PrototioGG_SPERSON.Classes.Compra.CompraItens;
using PrototioGG_SPERSON.Classes.Compra;
using PrototioGG_SPERSON.Classes.Plugin;

namespace PrototioGG_SPERSON.Telas.Cadastrar
{
    public partial class FrmCompraCadastro : UserControl
    {
        public FrmCompraCadastro()
        {
            InitializeComponent();

            ConfigurarGrid();
            carregarFornecedores();
            carregarProdutos();
            carregarFormPag();
        }

        BindingList<DTOProduto> listaprod = new BindingList<DTOProduto>();

        public void carregarFornecedores()
        {
            BusinessFornecedor business = new BusinessFornecedor();
            List<DTOFornecedor> lista = business.Listar();
            cboFornecedor.ValueMember = nameof(DTOFornecedor.ID);
            cboFornecedor.DisplayMember = nameof(DTOFornecedor.Fornecedor);
            cboFornecedor.DataSource = lista;
        }

        void ConfigurarGrid()
        {
            dgvCompraCadastro.AutoGenerateColumns = false;
            dgvCompraCadastro.DataSource = listaprod;
        }

        public void carregarProdutos()
        {
            DTOProduto dto = new DTOProduto();
            BusinessProduto bus = new BusinessProduto();
            List<DTOProduto> lista = bus.Listar();
            cboProdutos.ValueMember = nameof(dto.ID);
            cboProdutos.DisplayMember = nameof(dto.Nome);
            cboProdutos.DataSource = lista;

        }

        public void carregarFormPag()
        {
            List<string> listaPagamento = new List<string>();

            listaPagamento.Add("Debito");
            listaPagamento.Add("Credito");
            listaPagamento.Add("Dinheiro");

            cboFormPagamento.DataSource = listaPagamento;
        }

        decimal total = 0;
        int qtdtotal = 0;

        private void btnConfirmarCompraCadastro_Click(object sender, EventArgs e)
        {
            List<DTOCompraItem> list = new List<DTOCompraItem>();
            try
            {
                DTOCompra compra = new DTOCompra();
                compra.VL_Total = Convert.ToDecimal(lblTotalPreco.Text);
                compra.DT_Compra = DateTime.Now;
                compra.Forma_De_Pagamento = cboFormPagamento.Text;
                compra.Qtd_Produtos = qtdtotal;
                BusinessFornecedor forn = new BusinessFornecedor();
                DTOFornecedor dtoforn = cboFornecedor.SelectedItem as DTOFornecedor;

                compra.FK_Fornecedor = dtoforn.ID;
                BusinessCompra bus = new BusinessCompra();
                bus.Salvar(compra, listaprod.ToList());
                MessageBox.Show("Compra Confirmada com Sucesso");

            }
            catch (ArgumentException ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        private void btnAddCarrinho_Click(object sender, EventArgs e)
        {
            DTOProduto dto = cboProdutos.SelectedItem as DTOProduto;

            int qtd = Convert.ToInt32(nudQtdProduto.Text);
            decimal preco = 0;
            for (int i = 0; i < qtd; i++)
            {
                listaprod.Add(dto);
                preco += dto.Valor_Unit_Compra;
            }
            total += preco;
            qtdtotal += qtd;
            lblTotalPreco.Text = total.ToString();
        }



        private void cboProdutos_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                DTOProduto dto = cboProdutos.SelectedItem as DTOProduto;

                imgProduto.Image = ImagePlugin.ConverterParaImagem(dto.Imagem);
            }
            catch (Exception ex)
            {

            }
        }
    }
}
