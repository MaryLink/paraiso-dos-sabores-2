﻿namespace PrototioGG_SPERSON.Telas.Cadastrar
{
    partial class FrmCompraCadastro
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label5 = new System.Windows.Forms.Label();
            this.imgProduto = new System.Windows.Forms.PictureBox();
            this.btnAddCarrinho = new System.Windows.Forms.Button();
            this.btnConfirmarCompraCadastro = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.nudQtdProduto = new System.Windows.Forms.NumericUpDown();
            this.cboFormPagamento = new System.Windows.Forms.ComboBox();
            this.cboFornecedor = new System.Windows.Forms.ComboBox();
            this.cboProdutos = new System.Windows.Forms.ComboBox();
            this.dgvCompraCadastro = new System.Windows.Forms.DataGridView();
            this.Produto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Preço = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.lblTotalPreco = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.imgProduto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudQtdProduto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCompraCadastro)).BeginInit();
            this.SuspendLayout();
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Times New Roman", 18F, System.Drawing.FontStyle.Bold);
            this.label5.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label5.Location = new System.Drawing.Point(1060, 511);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(52, 35);
            this.label5.TabIndex = 34;
            this.label5.Text = "R$";
            // 
            // imgProduto
            // 
            this.imgProduto.BackColor = System.Drawing.Color.White;
            this.imgProduto.Location = new System.Drawing.Point(41, 81);
            this.imgProduto.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.imgProduto.Name = "imgProduto";
            this.imgProduto.Size = new System.Drawing.Size(207, 194);
            this.imgProduto.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgProduto.TabIndex = 33;
            this.imgProduto.TabStop = false;
            // 
            // btnAddCarrinho
            // 
            this.btnAddCarrinho.BackColor = System.Drawing.Color.RoyalBlue;
            this.btnAddCarrinho.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddCarrinho.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnAddCarrinho.Location = new System.Drawing.Point(568, 348);
            this.btnAddCarrinho.Margin = new System.Windows.Forms.Padding(4);
            this.btnAddCarrinho.Name = "btnAddCarrinho";
            this.btnAddCarrinho.Size = new System.Drawing.Size(141, 38);
            this.btnAddCarrinho.TabIndex = 32;
            this.btnAddCarrinho.Text = "Adicionar";
            this.btnAddCarrinho.UseVisualStyleBackColor = false;
            this.btnAddCarrinho.Click += new System.EventHandler(this.btnAddCarrinho_Click);
            // 
            // btnConfirmarCompraCadastro
            // 
            this.btnConfirmarCompraCadastro.BackColor = System.Drawing.Color.RoyalBlue;
            this.btnConfirmarCompraCadastro.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnConfirmarCompraCadastro.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnConfirmarCompraCadastro.Location = new System.Drawing.Point(916, 548);
            this.btnConfirmarCompraCadastro.Margin = new System.Windows.Forms.Padding(4);
            this.btnConfirmarCompraCadastro.Name = "btnConfirmarCompraCadastro";
            this.btnConfirmarCompraCadastro.Size = new System.Drawing.Size(292, 41);
            this.btnConfirmarCompraCadastro.TabIndex = 31;
            this.btnConfirmarCompraCadastro.Text = "Confirmar";
            this.btnConfirmarCompraCadastro.UseVisualStyleBackColor = false;
            this.btnConfirmarCompraCadastro.Click += new System.EventHandler(this.btnConfirmarCompraCadastro_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Times New Roman", 20.25F, ((System.Drawing.FontStyle)(((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic) 
                | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label4.Location = new System.Drawing.Point(33, 26);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(256, 39);
            this.label4.TabIndex = 30;
            this.label4.Text = "Realizar Compra";
            // 
            // nudQtdProduto
            // 
            this.nudQtdProduto.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nudQtdProduto.Location = new System.Drawing.Point(428, 353);
            this.nudQtdProduto.Margin = new System.Windows.Forms.Padding(4);
            this.nudQtdProduto.Name = "nudQtdProduto";
            this.nudQtdProduto.Size = new System.Drawing.Size(132, 26);
            this.nudQtdProduto.TabIndex = 29;
            // 
            // cboFormPagamento
            // 
            this.cboFormPagamento.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboFormPagamento.Font = new System.Drawing.Font("Arial Narrow", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboFormPagamento.FormattingEnabled = true;
            this.cboFormPagamento.Location = new System.Drawing.Point(429, 554);
            this.cboFormPagamento.Margin = new System.Windows.Forms.Padding(4);
            this.cboFormPagamento.Name = "cboFormPagamento";
            this.cboFormPagamento.Size = new System.Drawing.Size(280, 28);
            this.cboFormPagamento.TabIndex = 28;
            // 
            // cboFornecedor
            // 
            this.cboFornecedor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboFornecedor.Font = new System.Drawing.Font("Arial Narrow", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboFornecedor.FormattingEnabled = true;
            this.cboFornecedor.Location = new System.Drawing.Point(429, 517);
            this.cboFornecedor.Margin = new System.Windows.Forms.Padding(4);
            this.cboFornecedor.Name = "cboFornecedor";
            this.cboFornecedor.Size = new System.Drawing.Size(280, 28);
            this.cboFornecedor.TabIndex = 27;
            // 
            // cboProdutos
            // 
            this.cboProdutos.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboProdutos.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.cboProdutos.Font = new System.Drawing.Font("Arial Narrow", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboProdutos.FormattingEnabled = true;
            this.cboProdutos.Location = new System.Drawing.Point(428, 309);
            this.cboProdutos.Margin = new System.Windows.Forms.Padding(4);
            this.cboProdutos.Name = "cboProdutos";
            this.cboProdutos.Size = new System.Drawing.Size(280, 28);
            this.cboProdutos.TabIndex = 26;
            this.cboProdutos.SelectedIndexChanged += new System.EventHandler(this.cboProdutos_SelectedIndexChanged);
            // 
            // dgvCompraCadastro
            // 
            this.dgvCompraCadastro.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Times New Roman", 12F);
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvCompraCadastro.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvCompraCadastro.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCompraCadastro.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Produto,
            this.Preço});
            this.dgvCompraCadastro.GridColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.dgvCompraCadastro.Location = new System.Drawing.Point(772, 81);
            this.dgvCompraCadastro.Margin = new System.Windows.Forms.Padding(4);
            this.dgvCompraCadastro.Name = "dgvCompraCadastro";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Times New Roman", 8.25F);
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvCompraCadastro.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dgvCompraCadastro.RowHeadersVisible = false;
            this.dgvCompraCadastro.Size = new System.Drawing.Size(435, 212);
            this.dgvCompraCadastro.TabIndex = 25;
            // 
            // Produto
            // 
            this.Produto.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Produto.DataPropertyName = "Nome";
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Times New Roman", 8.25F);
            this.Produto.DefaultCellStyle = dataGridViewCellStyle2;
            this.Produto.HeaderText = "Produto";
            this.Produto.Name = "Produto";
            // 
            // Preço
            // 
            this.Preço.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Preço.DataPropertyName = "Valor_Unit_Compra";
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Times New Roman", 8.25F);
            this.Preço.DefaultCellStyle = dataGridViewCellStyle3;
            this.Preço.HeaderText = "Preço";
            this.Preço.Name = "Preço";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Times New Roman", 18F, System.Drawing.FontStyle.Bold);
            this.label6.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label6.Location = new System.Drawing.Point(284, 309);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(128, 35);
            this.label6.TabIndex = 24;
            this.label6.Text = "Produto:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Times New Roman", 18F, System.Drawing.FontStyle.Bold);
            this.label7.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label7.Location = new System.Drawing.Point(237, 517);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(170, 35);
            this.label7.TabIndex = 23;
            this.label7.Text = "Fornecedor:";
            // 
            // lblTotalPreco
            // 
            this.lblTotalPreco.AutoSize = true;
            this.lblTotalPreco.BackColor = System.Drawing.Color.Transparent;
            this.lblTotalPreco.Font = new System.Drawing.Font("Times New Roman", 18F, System.Drawing.FontStyle.Bold);
            this.lblTotalPreco.ForeColor = System.Drawing.Color.RoyalBlue;
            this.lblTotalPreco.Location = new System.Drawing.Point(1111, 511);
            this.lblTotalPreco.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTotalPreco.Name = "lblTotalPreco";
            this.lblTotalPreco.Size = new System.Drawing.Size(83, 35);
            this.lblTotalPreco.TabIndex = 22;
            this.lblTotalPreco.Text = "00,00";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Times New Roman", 18F, System.Drawing.FontStyle.Bold);
            this.label3.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label3.Location = new System.Drawing.Point(236, 348);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(173, 35);
            this.label3.TabIndex = 21;
            this.label3.Text = "Quantidade:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Times New Roman", 18F, System.Drawing.FontStyle.Bold);
            this.label2.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label2.Location = new System.Drawing.Point(909, 511);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(156, 35);
            this.label2.TabIndex = 20;
            this.label2.Text = "Valor total:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Times New Roman", 18F, System.Drawing.FontStyle.Bold);
            this.label1.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label1.Location = new System.Drawing.Point(105, 548);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(295, 35);
            this.label1.TabIndex = 19;
            this.label1.Text = "Forma de Pagamento:";
            // 
            // FrmCompraCadastro
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.BackgroundImage = global::PrototioGG_SPERSON.Properties.Resources._902893180_612x612;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Controls.Add(this.label5);
            this.Controls.Add(this.imgProduto);
            this.Controls.Add(this.btnAddCarrinho);
            this.Controls.Add(this.btnConfirmarCompraCadastro);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.nudQtdProduto);
            this.Controls.Add(this.cboFormPagamento);
            this.Controls.Add(this.cboFornecedor);
            this.Controls.Add(this.cboProdutos);
            this.Controls.Add(this.dgvCompraCadastro);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.lblTotalPreco);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.ForeColor = System.Drawing.Color.RoyalBlue;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "FrmCompraCadastro";
            this.Size = new System.Drawing.Size(1312, 629);
            ((System.ComponentModel.ISupportInitialize)(this.imgProduto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudQtdProduto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCompraCadastro)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.PictureBox imgProduto;
        private System.Windows.Forms.Button btnAddCarrinho;
        private System.Windows.Forms.Button btnConfirmarCompraCadastro;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown nudQtdProduto;
        private System.Windows.Forms.ComboBox cboFormPagamento;
        private System.Windows.Forms.ComboBox cboFornecedor;
        private System.Windows.Forms.ComboBox cboProdutos;
        private System.Windows.Forms.DataGridView dgvCompraCadastro;
        private System.Windows.Forms.DataGridViewTextBoxColumn Produto;
        private System.Windows.Forms.DataGridViewTextBoxColumn Preço;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label lblTotalPreco;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
    }
}
