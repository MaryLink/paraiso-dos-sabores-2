﻿namespace PrototioGG_SPERSON.Telas.Cadastrar
{
    partial class FrmFornecedorCadastro
    {
        /// <summary> 
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Designer de Componentes

        /// <summary> 
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.panelCadFORNECEDOR = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.btnSalvarFORNECEDOR = new System.Windows.Forms.Button();
            this.txtCEP = new System.Windows.Forms.MaskedTextBox();
            this.txtTelefone = new System.Windows.Forms.MaskedTextBox();
            this.txtCNPJ = new System.Windows.Forms.MaskedTextBox();
            this.txtCidadeFornecedor = new System.Windows.Forms.TextBox();
            this.txtBairroFornecedor = new System.Windows.Forms.TextBox();
            this.txtEstadoFornecedor = new System.Windows.Forms.TextBox();
            this.txtRuaFornecedor = new System.Windows.Forms.TextBox();
            this.txtEmailFornecedor = new System.Windows.Forms.TextBox();
            this.txtNomeFornecedor = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panelCadFORNECEDOR.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelCadFORNECEDOR
            // 
            this.panelCadFORNECEDOR.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.panelCadFORNECEDOR.BackgroundImage = global::PrototioGG_SPERSON.Properties.Resources._902893180_612x612;
            this.panelCadFORNECEDOR.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panelCadFORNECEDOR.Controls.Add(this.label5);
            this.panelCadFORNECEDOR.Controls.Add(this.btnSalvarFORNECEDOR);
            this.panelCadFORNECEDOR.Controls.Add(this.txtCEP);
            this.panelCadFORNECEDOR.Controls.Add(this.txtTelefone);
            this.panelCadFORNECEDOR.Controls.Add(this.txtCNPJ);
            this.panelCadFORNECEDOR.Controls.Add(this.txtCidadeFornecedor);
            this.panelCadFORNECEDOR.Controls.Add(this.txtBairroFornecedor);
            this.panelCadFORNECEDOR.Controls.Add(this.txtEstadoFornecedor);
            this.panelCadFORNECEDOR.Controls.Add(this.txtRuaFornecedor);
            this.panelCadFORNECEDOR.Controls.Add(this.txtEmailFornecedor);
            this.panelCadFORNECEDOR.Controls.Add(this.txtNomeFornecedor);
            this.panelCadFORNECEDOR.Controls.Add(this.label10);
            this.panelCadFORNECEDOR.Controls.Add(this.label9);
            this.panelCadFORNECEDOR.Controls.Add(this.label8);
            this.panelCadFORNECEDOR.Controls.Add(this.label7);
            this.panelCadFORNECEDOR.Controls.Add(this.label6);
            this.panelCadFORNECEDOR.Controls.Add(this.label4);
            this.panelCadFORNECEDOR.Controls.Add(this.label3);
            this.panelCadFORNECEDOR.Controls.Add(this.label2);
            this.panelCadFORNECEDOR.Controls.Add(this.label1);
            this.panelCadFORNECEDOR.Location = new System.Drawing.Point(0, 0);
            this.panelCadFORNECEDOR.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panelCadFORNECEDOR.Name = "panelCadFORNECEDOR";
            this.panelCadFORNECEDOR.Size = new System.Drawing.Size(1312, 629);
            this.panelCadFORNECEDOR.TabIndex = 0;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Book Antiqua", 27.75F, ((System.Drawing.FontStyle)(((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic) 
                | System.Drawing.FontStyle.Underline))));
            this.label5.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label5.Location = new System.Drawing.Point(4, 22);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(253, 56);
            this.label5.TabIndex = 20;
            this.label5.Text = "Fornecedor";
            // 
            // btnSalvarFORNECEDOR
            // 
            this.btnSalvarFORNECEDOR.BackColor = System.Drawing.Color.RoyalBlue;
            this.btnSalvarFORNECEDOR.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSalvarFORNECEDOR.Font = new System.Drawing.Font("Times New Roman", 18F, System.Drawing.FontStyle.Bold);
            this.btnSalvarFORNECEDOR.ForeColor = System.Drawing.Color.White;
            this.btnSalvarFORNECEDOR.Location = new System.Drawing.Point(845, 444);
            this.btnSalvarFORNECEDOR.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnSalvarFORNECEDOR.Name = "btnSalvarFORNECEDOR";
            this.btnSalvarFORNECEDOR.Size = new System.Drawing.Size(319, 47);
            this.btnSalvarFORNECEDOR.TabIndex = 11;
            this.btnSalvarFORNECEDOR.Text = "Salvar";
            this.btnSalvarFORNECEDOR.UseVisualStyleBackColor = false;
            this.btnSalvarFORNECEDOR.Click += new System.EventHandler(this.btnSalvarFORNECEDOR_Click);
            // 
            // txtCEP
            // 
            this.txtCEP.Font = new System.Drawing.Font("Arial Narrow", 12F);
            this.txtCEP.Location = new System.Drawing.Point(848, 364);
            this.txtCEP.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtCEP.Mask = "00000000";
            this.txtCEP.Name = "txtCEP";
            this.txtCEP.Size = new System.Drawing.Size(317, 30);
            this.txtCEP.TabIndex = 10;
            // 
            // txtTelefone
            // 
            this.txtTelefone.Font = new System.Drawing.Font("Arial Narrow", 12F);
            this.txtTelefone.Location = new System.Drawing.Point(235, 330);
            this.txtTelefone.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtTelefone.Mask = "(00) 0000 - 0000";
            this.txtTelefone.Name = "txtTelefone";
            this.txtTelefone.Size = new System.Drawing.Size(317, 30);
            this.txtTelefone.TabIndex = 5;
            // 
            // txtCNPJ
            // 
            this.txtCNPJ.Font = new System.Drawing.Font("Arial Narrow", 12F);
            this.txtCNPJ.Location = new System.Drawing.Point(235, 290);
            this.txtCNPJ.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtCNPJ.Mask = "00.000.000/0000-00";
            this.txtCNPJ.Name = "txtCNPJ";
            this.txtCNPJ.Size = new System.Drawing.Size(317, 30);
            this.txtCNPJ.TabIndex = 4;
            // 
            // txtCidadeFornecedor
            // 
            this.txtCidadeFornecedor.Font = new System.Drawing.Font("Arial Narrow", 12F);
            this.txtCidadeFornecedor.Location = new System.Drawing.Point(848, 208);
            this.txtCidadeFornecedor.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtCidadeFornecedor.Name = "txtCidadeFornecedor";
            this.txtCidadeFornecedor.Size = new System.Drawing.Size(317, 30);
            this.txtCidadeFornecedor.TabIndex = 6;
            // 
            // txtBairroFornecedor
            // 
            this.txtBairroFornecedor.Font = new System.Drawing.Font("Arial Narrow", 12F);
            this.txtBairroFornecedor.Location = new System.Drawing.Point(848, 247);
            this.txtBairroFornecedor.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtBairroFornecedor.Name = "txtBairroFornecedor";
            this.txtBairroFornecedor.Size = new System.Drawing.Size(317, 30);
            this.txtBairroFornecedor.TabIndex = 7;
            // 
            // txtEstadoFornecedor
            // 
            this.txtEstadoFornecedor.Font = new System.Drawing.Font("Arial Narrow", 12F);
            this.txtEstadoFornecedor.Location = new System.Drawing.Point(848, 287);
            this.txtEstadoFornecedor.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtEstadoFornecedor.Name = "txtEstadoFornecedor";
            this.txtEstadoFornecedor.Size = new System.Drawing.Size(317, 30);
            this.txtEstadoFornecedor.TabIndex = 8;
            // 
            // txtRuaFornecedor
            // 
            this.txtRuaFornecedor.Font = new System.Drawing.Font("Arial Narrow", 12F);
            this.txtRuaFornecedor.Location = new System.Drawing.Point(848, 326);
            this.txtRuaFornecedor.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtRuaFornecedor.Name = "txtRuaFornecedor";
            this.txtRuaFornecedor.Size = new System.Drawing.Size(317, 30);
            this.txtRuaFornecedor.TabIndex = 9;
            // 
            // txtEmailFornecedor
            // 
            this.txtEmailFornecedor.Font = new System.Drawing.Font("Arial Narrow", 12F);
            this.txtEmailFornecedor.Location = new System.Drawing.Point(235, 252);
            this.txtEmailFornecedor.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtEmailFornecedor.Name = "txtEmailFornecedor";
            this.txtEmailFornecedor.Size = new System.Drawing.Size(317, 30);
            this.txtEmailFornecedor.TabIndex = 2;
            // 
            // txtNomeFornecedor
            // 
            this.txtNomeFornecedor.Font = new System.Drawing.Font("Arial Narrow", 12F);
            this.txtNomeFornecedor.Location = new System.Drawing.Point(235, 214);
            this.txtNomeFornecedor.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtNomeFornecedor.Name = "txtNomeFornecedor";
            this.txtNomeFornecedor.Size = new System.Drawing.Size(317, 30);
            this.txtNomeFornecedor.TabIndex = 1;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Times New Roman", 18F, System.Drawing.FontStyle.Bold);
            this.label10.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label10.Location = new System.Drawing.Point(91, 330);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(130, 35);
            this.label10.TabIndex = 9;
            this.label10.Text = "Telefone:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Times New Roman", 18F, System.Drawing.FontStyle.Bold);
            this.label9.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label9.Location = new System.Drawing.Point(720, 287);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(113, 35);
            this.label9.TabIndex = 8;
            this.label9.Text = "Estado:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Times New Roman", 18F, System.Drawing.FontStyle.Bold);
            this.label8.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label8.Location = new System.Drawing.Point(121, 252);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(99, 35);
            this.label8.TabIndex = 7;
            this.label8.Text = "Email:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Times New Roman", 18F, System.Drawing.FontStyle.Bold);
            this.label7.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label7.Location = new System.Drawing.Point(747, 367);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(86, 35);
            this.label7.TabIndex = 6;
            this.label7.Text = "CEP:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Times New Roman", 18F, System.Drawing.FontStyle.Bold);
            this.label6.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label6.Location = new System.Drawing.Point(720, 208);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(116, 35);
            this.label6.TabIndex = 5;
            this.label6.Text = "Cidade:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Times New Roman", 18F, System.Drawing.FontStyle.Bold);
            this.label4.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label4.Location = new System.Drawing.Point(727, 247);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(106, 35);
            this.label4.TabIndex = 3;
            this.label4.Text = "Bairro:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Times New Roman", 18F, System.Drawing.FontStyle.Bold);
            this.label3.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label3.Location = new System.Drawing.Point(755, 326);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 35);
            this.label3.TabIndex = 2;
            this.label3.Text = "Rua:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Times New Roman", 18F, System.Drawing.FontStyle.Bold);
            this.label2.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label2.Location = new System.Drawing.Point(117, 293);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(103, 35);
            this.label2.TabIndex = 1;
            this.label2.Text = "CNPJ:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Times New Roman", 18F, System.Drawing.FontStyle.Bold);
            this.label1.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label1.Location = new System.Drawing.Point(121, 214);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(99, 35);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nome:";
            // 
            // FrmFornecedorCadastro
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panelCadFORNECEDOR);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "FrmFornecedorCadastro";
            this.Size = new System.Drawing.Size(1312, 629);
            this.panelCadFORNECEDOR.ResumeLayout(false);
            this.panelCadFORNECEDOR.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelCadFORNECEDOR;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtCidadeFornecedor;
        private System.Windows.Forms.TextBox txtBairroFornecedor;
        private System.Windows.Forms.TextBox txtEstadoFornecedor;
        private System.Windows.Forms.TextBox txtRuaFornecedor;
        private System.Windows.Forms.TextBox txtEmailFornecedor;
        private System.Windows.Forms.TextBox txtNomeFornecedor;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button btnSalvarFORNECEDOR;
        private System.Windows.Forms.MaskedTextBox txtCEP;
        private System.Windows.Forms.MaskedTextBox txtTelefone;
        private System.Windows.Forms.MaskedTextBox txtCNPJ;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.Label label5;
    }
}
