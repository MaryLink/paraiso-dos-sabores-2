﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PrototioGG_SPERSON.Classes.Folha_de_Pagamento;
using PrototioGG_SPERSON.Classes.Funcionario.ViewsFuncionario;
using PrototioGG_SPERSON.Classes.Folha_de_Pagamento.FGTS;
using PrototioGG_SPERSON.Classes.Folha_de_Pagamento.Inss;
using PrototioGG_SPERSON.Classes.Folha_de_Pagamento.IR;
using PrototioGG_SPERSON.Classes.Folha_de_Pagamento.Comissao;

namespace PrototioGG_SPERSON.Telas.Cadastrar
{
    public partial class FrmFolhaDePagamento : UserControl
    {
        public FrmFolhaDePagamento()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            BusinessVwFuncionario buss = new BusinessVwFuncionario();
            List<DTOVwFuncionario> lista = new List<DTOVwFuncionario>();
            CalcularFolhaDePagamento calc = new CalcularFolhaDePagamento();
            CalcularFGTS fgtsCalc = new CalcularFGTS();
            CalcularINSS inssCalc = new CalcularINSS();
            CalculoIR irCalc = new CalculoIR();
            CalculoComissao comissaoCalc = new CalculoComissao();

            decimal salarioBruto, inss, fgts, ir, vr, vt, comissao, percentComissao = 0;


            lista = buss.Listar();

            List<DTOGridFolha> listadgv = new List<DTOGridFolha>();

            foreach (DTOVwFuncionario funcionario in lista)
            {

                decimal salarioLiquido = 0;
                //salario base/bruto 
                salarioBruto = funcionario.ValorSalario;
                inss = inssCalc.SaldoINSS(salarioBruto);
                fgts = fgtsCalc.CalculoFGTS(salarioBruto);
                ir = irCalc.IR(salarioBruto);
                vr = fgtsCalc.CalcularValeRefeicao(salarioBruto);
                vt = fgtsCalc.CalcularValeTransporte(salarioBruto);
                comissao = comissaoCalc.CalcularComissao(percentComissao, funcionario.Nome);

                salarioBruto = Math.Round(salarioBruto, 2);
                inss = Math.Round(inss, 2);
                fgts = Math.Round(fgts, 2);
                ir = Math.Round(ir, 2);
                vr = Math.Round(vr, 2);
                vt = Math.Round(vt, 2);
                comissao = Math.Round(comissao, 2);
                salarioLiquido = vr - vt - ir - fgts - inss + comissao + salarioBruto;

                DTOGridFolha gridlist = new DTOGridFolha();
                gridlist.Funcionario = funcionario.Nome;
                gridlist.Comissao_Vendas = comissao;
                //Hora Extra :::::
                //gridlist.HX
                gridlist.INSS = inss;
                gridlist.IR = ir;
                gridlist.Salario_Bruto = funcionario.ValorSalario;
                gridlist.Salario_Liquido = salarioLiquido;
                gridlist.VR = vr;
                gridlist.VT = vt;

                listadgv.Add(gridlist);
            }

            dgvFolhadePagamento.AutoGenerateColumns = false;
            dgvFolhadePagamento.DataSource = listadgv;
        }
    }
}
