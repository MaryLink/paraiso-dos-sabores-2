﻿namespace PrototioGG_SPERSON.Telas.Consultar
{
    partial class FrmGerenciarEstoque
    {
        /// <summary> 
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Designer de Componentes

        /// <summary> 
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelGerenciarEstoque = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.nudMax = new System.Windows.Forms.NumericUpDown();
            this.nudMin = new System.Windows.Forms.NumericUpDown();
            this.button1 = new System.Windows.Forms.Button();
            this.txtIDPROD = new System.Windows.Forms.TextBox();
            this.txtBuscarProduto = new System.Windows.Forms.TextBox();
            this.btnBuscar = new System.Windows.Forms.Button();
            this.dgvEstoque = new System.Windows.Forms.DataGridView();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label = new System.Windows.Forms.Label();
            this.panelGerenciarEstoque.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudMax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudMin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEstoque)).BeginInit();
            this.SuspendLayout();
            // 
            // panelGerenciarEstoque
            // 
            this.panelGerenciarEstoque.BackgroundImage = global::PrototioGG_SPERSON.Properties.Resources._902893180_612x6121;
            this.panelGerenciarEstoque.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panelGerenciarEstoque.Controls.Add(this.label3);
            this.panelGerenciarEstoque.Controls.Add(this.label2);
            this.panelGerenciarEstoque.Controls.Add(this.label1);
            this.panelGerenciarEstoque.Controls.Add(this.nudMax);
            this.panelGerenciarEstoque.Controls.Add(this.nudMin);
            this.panelGerenciarEstoque.Controls.Add(this.button1);
            this.panelGerenciarEstoque.Controls.Add(this.txtIDPROD);
            this.panelGerenciarEstoque.Controls.Add(this.txtBuscarProduto);
            this.panelGerenciarEstoque.Controls.Add(this.btnBuscar);
            this.panelGerenciarEstoque.Controls.Add(this.dgvEstoque);
            this.panelGerenciarEstoque.Controls.Add(this.label);
            this.panelGerenciarEstoque.Location = new System.Drawing.Point(0, 0);
            this.panelGerenciarEstoque.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panelGerenciarEstoque.Name = "panelGerenciarEstoque";
            this.panelGerenciarEstoque.Size = new System.Drawing.Size(1284, 634);
            this.panelGerenciarEstoque.TabIndex = 0;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label3.Location = new System.Drawing.Point(728, 79);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(33, 17);
            this.label3.TabIndex = 23;
            this.label3.Text = "Max";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label2.Location = new System.Drawing.Point(647, 79);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(30, 17);
            this.label2.TabIndex = 22;
            this.label2.Text = "Min";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label1.Location = new System.Drawing.Point(597, 71);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(21, 17);
            this.label1.TabIndex = 21;
            this.label1.Text = "ID";
            // 
            // nudMax
            // 
            this.nudMax.Location = new System.Drawing.Point(732, 100);
            this.nudMax.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.nudMax.Name = "nudMax";
            this.nudMax.Size = new System.Drawing.Size(75, 22);
            this.nudMax.TabIndex = 20;
            // 
            // nudMin
            // 
            this.nudMin.Location = new System.Drawing.Point(649, 100);
            this.nudMin.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.nudMin.Name = "nudMin";
            this.nudMin.Size = new System.Drawing.Size(75, 22);
            this.nudMin.TabIndex = 19;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(815, 96);
            this.button1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(100, 28);
            this.button1.TabIndex = 18;
            this.button1.Text = "Alterar";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // txtIDPROD
            // 
            this.txtIDPROD.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtIDPROD.Location = new System.Drawing.Point(600, 95);
            this.txtIDPROD.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtIDPROD.Name = "txtIDPROD";
            this.txtIDPROD.ReadOnly = true;
            this.txtIDPROD.Size = new System.Drawing.Size(39, 29);
            this.txtIDPROD.TabIndex = 17;
            // 
            // txtBuscarProduto
            // 
            this.txtBuscarProduto.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBuscarProduto.Location = new System.Drawing.Point(29, 95);
            this.txtBuscarProduto.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtBuscarProduto.Name = "txtBuscarProduto";
            this.txtBuscarProduto.Size = new System.Drawing.Size(288, 29);
            this.txtBuscarProduto.TabIndex = 16;
            // 
            // btnBuscar
            // 
            this.btnBuscar.BackColor = System.Drawing.Color.RoyalBlue;
            this.btnBuscar.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnBuscar.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnBuscar.Location = new System.Drawing.Point(325, 96);
            this.btnBuscar.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnBuscar.Name = "btnBuscar";
            this.btnBuscar.Size = new System.Drawing.Size(100, 28);
            this.btnBuscar.TabIndex = 15;
            this.btnBuscar.Text = "Consultar";
            this.btnBuscar.UseVisualStyleBackColor = false;
            // 
            // dgvEstoque
            // 
            this.dgvEstoque.AllowUserToDeleteRows = false;
            this.dgvEstoque.BackgroundColor = System.Drawing.Color.LightPink;
            this.dgvEstoque.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvEstoque.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvEstoque.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column5,
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4});
            this.dgvEstoque.GridColor = System.Drawing.SystemColors.HotTrack;
            this.dgvEstoque.Location = new System.Drawing.Point(29, 171);
            this.dgvEstoque.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dgvEstoque.Name = "dgvEstoque";
            this.dgvEstoque.ReadOnly = true;
            this.dgvEstoque.RowHeadersVisible = false;
            this.dgvEstoque.Size = new System.Drawing.Size(1225, 391);
            this.dgvEstoque.TabIndex = 14;
            // 
            // Column5
            // 
            this.Column5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Column5.DataPropertyName = "ID";
            this.Column5.HeaderText = "ID";
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            // 
            // Column1
            // 
            this.Column1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Column1.DataPropertyName = "Produto";
            this.Column1.HeaderText = "Produto";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            // 
            // Column2
            // 
            this.Column2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Column2.DataPropertyName = "Qtd_Max";
            this.Column2.HeaderText = "Quantidade Maxima";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            // 
            // Column3
            // 
            this.Column3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Column3.DataPropertyName = "Qtd_Min";
            this.Column3.HeaderText = "Quantidade Minima";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            // 
            // Column4
            // 
            this.Column4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Column4.DataPropertyName = "Qtd_Atual";
            this.Column4.HeaderText = "Quantidade Atual";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            // 
            // label
            // 
            this.label.AutoSize = true;
            this.label.BackColor = System.Drawing.Color.Transparent;
            this.label.Font = new System.Drawing.Font("Book Antiqua", 27.75F, ((System.Drawing.FontStyle)(((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic) 
                | System.Drawing.FontStyle.Underline))));
            this.label.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label.Location = new System.Drawing.Point(39, 16);
            this.label.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label.Name = "label";
            this.label.Size = new System.Drawing.Size(589, 56);
            this.label.TabIndex = 5;
            this.label.Text = "Gerenciamento de Estoque ";
            // 
            // FrmGerenciarEstoque
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panelGerenciarEstoque);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "FrmGerenciarEstoque";
            this.Size = new System.Drawing.Size(1284, 634);
            this.panelGerenciarEstoque.ResumeLayout(false);
            this.panelGerenciarEstoque.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudMax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudMin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEstoque)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelGerenciarEstoque;
        private System.Windows.Forms.Label label;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown nudMax;
        private System.Windows.Forms.NumericUpDown nudMin;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox txtIDPROD;
        private System.Windows.Forms.TextBox txtBuscarProduto;
        private System.Windows.Forms.Button btnBuscar;
        private System.Windows.Forms.DataGridView dgvEstoque;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
    }
}
