﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PrototioGG_SPERSON.Classes.Venda.VendaItens.View;

namespace PrototioGG_SPERSON.Telas.Consultar
{
    public partial class FrmVendaConsultar : UserControl
    {
        public FrmVendaConsultar()
        {
            InitializeComponent();
        }

        private void btnBuscarDepartamento_Click(object sender, EventArgs e)
        {
            try
            {
                DTOvwVenda dto = new DTOvwVenda();
                dto.nm_produto = txtVenda.Text.Trim();
                BusinessvwVenda buss = new BusinessvwVenda();
                List<DTOvwVenda> lista = buss.ConsultarView(dto);

                dgvVenda.AutoGenerateColumns = false;
                dgvVenda.DataSource = lista;
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        private void dgvVenda_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
