﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PrototioGG_SPERSON.Classes.Compra.CompraItens.View;

namespace PrototioGG_SPERSON.Telas.Consultar
{
    public partial class FrmCompraConsultar : UserControl
    {
        public FrmCompraConsultar()
        {
            InitializeComponent();
        }

        private void btnBuscarDepartamento_Click(object sender, EventArgs e)
        {
            try
            {

                DTOvwCompra dto = new DTOvwCompra();
                dto.Nome_Produto = txtVenda.Text.Trim();
                BusinessvwCompra buss = new BusinessvwCompra();
                List<DTOvwCompra> lista = buss.ConsultarView(dto);

                dgvVenda.AutoGenerateColumns = false;
                dgvVenda.DataSource = lista;
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }
    }
}
