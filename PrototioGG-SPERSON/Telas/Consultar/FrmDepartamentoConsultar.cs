﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PrototioGG_SPERSON.DB.Departamento;

namespace PrototioGG_SPERSON.Telas.Consultar
{
    public partial class FrmDepartamentoConsultar : UserControl
    {
        public FrmDepartamentoConsultar()
        {
            InitializeComponent();
        }

        private void btnBuscarDepartamento_Click(object sender, EventArgs e)
        {
            string departamento = txtBuscarDepartamento.Text;
            BusinessDepartamento bus = new BusinessDepartamento();
            List<DTODepartamento> lista = bus.Consultar(departamento);

            dgvDepartamento.AutoGenerateColumns = false;
            dgvDepartamento.DataSource = lista;

        }

        private void btnBuscarDepartamento_Click_1(object sender, EventArgs e)
        {
            try
            {
                string departamento = txtBuscarDepartamento.Text;
                BusinessDepartamento bus = new BusinessDepartamento();
                List<DTODepartamento> lista = bus.Consultar(departamento);

                dgvDepartamento.AutoGenerateColumns = false;
                dgvDepartamento.DataSource = lista;
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        private void dgvDepartamento_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                DTODepartamento departamento = dgvDepartamento.Rows[e.RowIndex].DataBoundItem as DTODepartamento;
                if (e.ColumnIndex == 02)
                {


                    DialogResult r = MessageBox.Show($"Deseja realmente excluir o departamento {departamento.ID}?", "Paraiso dos Sabores",
                                        MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                    if (r == DialogResult.Yes)
                    {
                        BusinessDepartamento business = new BusinessDepartamento();
                        business.Deletar(departamento.ID);

                        btnBuscarDepartamento_Click(null, null);
                    }
                }


            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Paraiso dos sabores",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreru um erro, tente mais tarde.", "Paraiso dos Sabores",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
