﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PrototioGG_SPERSON.Classes.Funcionario.ViewsFuncionario;
using PrototioGG_SPERSON.DB.Funcionario;
using PrototioGG_SPERSON.Classes.Plugin;
using PrototioGG_SPERSON.Telas.Alterar;

namespace PrototioGG_SPERSON.Telas.Consultar
{
    public partial class FrmFuncionariosConsultar : UserControl
    {
        public FrmFuncionariosConsultar()
        {
            InitializeComponent();
        }

        private void btnBuscarFuncionario_Click(object sender, EventArgs e)
         {
            try
            {
                DTOVwFuncionario dto = new DTOVwFuncionario();

                dto.Nome = txtBuscarFuncionario.Text.Trim();

                List<DTOVwFuncionario> list = new List<DTOVwFuncionario>();

                BusinessVwFuncionario bus = new BusinessVwFuncionario();
                list = bus.ConsultarView(dto);
                dgvFuncionarios.AutoGenerateColumns = false;
                dgvFuncionarios.DataSource = list;
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        private void dgvFuncionarios_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {

            DTOVwFuncionario func = dgvFuncionarios.CurrentRow.DataBoundItem as DTOVwFuncionario;

            DTOFuncionario funcionario = new DTOFuncionario();
            funcionario.ID = func.ID;



            if (e.ColumnIndex == 13)
            {
                DialogResult r = MessageBox.Show($"Deseja realmente excluir o funcionario {funcionario.ID}?", "GG's Person",
                                    MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (r == DialogResult.Yes)
                {
                    BusinessFuncionario business = new BusinessFuncionario();
                    business.Remover(funcionario.ID);

                    btnBuscarFuncionario_Click(null, null);
                }
            }
            //if (e.ColumnIndex == 13)
            //{
            //    FrmFuncionarioAlterar tela = new FrmFuncionarioAlterar();
            //    tela.LoadScreen(funcionario.ID);

            //    panelFuncionariosConsultar.Controls.Clear();

            //    panelFuncionariosConsultar.Controls.Add(tela);
            //}

            pictureBox1.Image = ImagePlugin.ConverterParaImagem(func.Imagem);

            }
            catch (Exception ex)
            {

            }
        }
    }
}
