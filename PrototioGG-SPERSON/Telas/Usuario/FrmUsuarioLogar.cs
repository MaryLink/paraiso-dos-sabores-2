﻿using PrototioGG_SPERSON.Classes.Folha_de_Pagamento.Ponto;
using PrototioGG_SPERSON.Classes.Funcionario.BaterPonto;
using PrototioGG_SPERSON.DB.Funcionario;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PrototioGG_SPERSON.Telas.Logar
{
    public partial class FrmUsuarioLogar : Form
    {
        public FrmUsuarioLogar()
        {
            InitializeComponent();
            imageOlhoComCorte.Visible = false;
            imageOlho1.Visible = true;
        }

        private void btnEntrar_Click(object sender, EventArgs e)
        {
            try
            {
                Efetuar_Login();
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }


        }


        public void Efetuar_Login()
        {
            DTOFuncionario dto = new DTOFuncionario();
            dto.UserName = txtUsername.Text.Trim();
            dto.Password = txtSenha.Text.Trim();

            BusinessFuncionario bus = new BusinessFuncionario();
            dto = bus.Logou(dto);
            if (dto != null)
            {
                SessãoUsuario.UsuarioLogado = dto;
                DTOPonto dto2 = new DTOPonto();
                PontoDoDia.PontoDia = dto2;

                TelaInicial telinha = new TelaInicial();
                telinha.Show();
                this.Hide();
            }

        }

        private void imageOlhoComCorte_Click(object sender, EventArgs e)
        {
            imageOlho1.Visible = true;
            imageOlhoComCorte.Visible = false;
            txtSenha.UseSystemPasswordChar = true;
        }

        private void imageOlho1_Click(object sender, EventArgs e)
        {
            imageOlho1.Visible = false;
            imageOlhoComCorte.Visible = true;
            txtSenha.UseSystemPasswordChar = false;
           
        }
    }
}
